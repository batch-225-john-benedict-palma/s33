
//1.

fetch('https://jsonplaceholder.typicode.com/todos')
.then((response) => response.json())
.then((json) => console.log(json));


//5.

fetch('https://jsonplaceholder.typicode.com/todos/1')
.then((response) => response.json())
.then((json) => console.log(json));

//6.

fetch('https://jsonplaceholder.typicode.com/todos', {
	method: 'POST',
	headers: {
  	'Content-type': 'application/json',
	},
		body: JSON.stringify({
		userId: 1201,
		title: 'New One',
		completed: false
	})
})
.then((response) => response.json())
.then((json) => console.log(json));




fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
		id: 1,
	  	title: 'Updated post',
	  	description: 'An updated post',
	  	status : 'incomplete',
	  	date_completed : 'N/A',
	  	user_Id: '102222'
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


fetch('https://jsonplaceholder.typicode.com/todos/2', {
	method: 'PATCH',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
	  	id: 1,
	  	title: 'Patched post',
	  	description: 'A patched post',
	  	status : 'completed',
	  	status_changed_date : '01/23/2023'
	  	
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


fetch('https://jsonplaceholder.typicode.com/todos/1', {
	method: 'PUT',
	headers: {
  	'Content-type': 'application/json',
	},
	body: JSON.stringify({
		id: 1,
	  	title: 'Updated post',
	  	description: 'An updated post',
	  	status : 'completed',
	  	date_completed : '1/23/2023',
	  	user_Id: '102222'
	})
})
.then((response) => response.json())
.then((json) => console.log(json));


fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'DELETE'
});